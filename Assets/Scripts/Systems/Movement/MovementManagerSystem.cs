﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// контролирует какую из систем в данный момент нужно использовать для передвижения
public class MovementManagerSystem
{
    FreeMovementSystem freeMovementSystem = new FreeMovementSystem();
    CombatMovementSystem combatMovementSystem = new CombatMovementSystem();

    MovementSystem currentMovementSystem;

    MovementManagerSystem()
    {
        currentMovementSystem = freeMovementSystem;
    }
}
