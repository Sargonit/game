﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatMovementSystem : MovementSystem
{
    public override void bounceBack(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void dodgeLeft(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void dodgeRight(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void jumpBackward(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void jumpForward(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void longJumpForward(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void moveBackward(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void moveForward(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void rotate(GameObject character, float direction)
    {
        throw new System.NotImplementedException();
    }

    public override void runForward(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void turnAroundByRun(GameObject character)
    {
        throw new System.NotImplementedException();
    }

    public override void turnAroundByWalk(GameObject character)
    {
        throw new System.NotImplementedException();
    }
}
