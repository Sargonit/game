﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementSystem
{
    public abstract void moveForward(GameObject character); // Идти вперед
    public abstract void runForward(GameObject character); // Бежать вперед
    public abstract void turnAroundByWalk(GameObject character); // Разворот на 180 градусов при ходьбе
    public abstract void turnAroundByRun(GameObject character); // Разворот на 180 градусов при беге
    public abstract void moveBackward(GameObject character); // Идти спиной
    public abstract void rotate(GameObject character, float direction); // Поворот на месте или во время движения вперед-назад.
    public abstract void jumpForward(GameObject character); // Прыжок вперед во время хотьбы
    public abstract void jumpBackward(GameObject character); // Прыжок назад
    public abstract void dodgeLeft(GameObject character); // Уворот влево
    public abstract void dodgeRight(GameObject character); // Уворот вправо
    public abstract void longJumpForward(GameObject character); // Прыжок вперед во время бега
    public abstract void bounceBack(GameObject character); // Отскок назад
}
